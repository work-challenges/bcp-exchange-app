package pe.com.bcp.exchange.currency.data.repository

import kotlinx.coroutines.flow.Flow
import pe.com.bcp.exchange.core.domain.repository.CurrencyRepository
import pe.com.bcp.exchange.currency.data.datasource.GetExchangeRatesDataSource

class CurrencyRepositoryImpl(
    private val getExchangeRatesDataSource: GetExchangeRatesDataSource
) : CurrencyRepository {
    override fun getExchangeRates(): Flow<Map<String, Map<String, Triple<String, Double, String>>>> {
        return getExchangeRatesDataSource.invoke()
    }
}