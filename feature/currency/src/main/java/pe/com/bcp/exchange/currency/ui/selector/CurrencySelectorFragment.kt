package pe.com.bcp.exchange.currency.ui.selector

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import pe.com.bcp.exchange.core.ui.CurrencyRatesItem
import pe.com.bcp.exchange.core.ui.viewmodel.MainViewModel
import pe.com.bcp.exchange.currency.databinding.FragmentCurrencySelectorBinding
import pe.com.bcp.exchange.currency.ui.selector.adapter.ExchangeRateAdapter

class CurrencySelectorFragment : Fragment() {
    private lateinit var binding: FragmentCurrencySelectorBinding
    private val viewModel: CurrencySelectorViewModel by viewModel()
    private val mainViewModel: MainViewModel by sharedViewModel()
    private val args: CurrencySelectorFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCurrencySelectorBinding.inflate(inflater, container, false)
        subscribeUi()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.findCurrencyRates(args.currency)
    }

    private fun subscribeUi() {
        viewModel.rates.observe(viewLifecycleOwner) { list ->
            setupRecyclerView(list)
        }
    }

    private fun setupRecyclerView(list: List<CurrencyRatesItem>?) {
        val exchangeRateAdapter = ExchangeRateAdapter(::navigate)
        exchangeRateAdapter.submitList(list)
        with(binding.currencyRecycler) {
            addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
            adapter = exchangeRateAdapter
        }
    }

    private fun navigate(it: CurrencyRatesItem) {
        mainViewModel.updateSelectedCurrency(it)
        CurrencySelectorFragmentDirections.actionSelectorToConverter().apply {
            findNavController().navigate(this)
        }
    }
}