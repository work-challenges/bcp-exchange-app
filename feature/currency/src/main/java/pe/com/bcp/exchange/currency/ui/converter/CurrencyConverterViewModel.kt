package pe.com.bcp.exchange.currency.ui.converter

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.parameter.parametersOf
import pe.com.bcp.exchange.core.domain.entity.Currency
import pe.com.bcp.exchange.core.domain.entity.Exchange
import pe.com.bcp.exchange.core.domain.entity.ExchangeRate
import pe.com.bcp.exchange.core.ui.viewmodel.BaseViewModel

class CurrencyConverterViewModel : BaseViewModel(), KoinComponent {
    private lateinit var rates: Map<String, Map<String, Triple<String, Double, String>>>
    private var sender: Currency? = null
    private var receiver: Currency? = null
    private val _currentRate = MutableLiveData<Double>()
    private val _computedResult = MutableLiveData<Double>()
    private val exchange: Exchange by inject {
        parametersOf(exchangeRate)
    }
    private val exchangeRate: ExchangeRate by inject {
        parametersOf(rates)
    }
    val computedResult: LiveData<Double> get() = _computedResult
    val currentRate: LiveData<Double> get() = _currentRate

    fun submitRates(data: Map<String, Map<String, Triple<String, Double, String>>>) {
        this.rates = data
    }

    fun updateSender(sender: Currency) {
        this.sender = sender
    }

    fun updateReceiver(receiver: Currency) {
        this.receiver = receiver
    }

    fun calculateCurrentRate() {
        if (sender != null && receiver != null) {
            exchangeRate.getExchangeRate(sender!!.name, receiver!!.name).let { rate ->
                _currentRate.postValue(rate)
            }
        }
    }

    fun compute(senderAmount: Double) {
        if (sender != null && receiver != null) {
            exchange.compute(senderAmount, sender!!.name, receiver!!.name).let { result ->
                calculateCurrentRate()
                _computedResult.postValue(result)
            }
        }
    }
}