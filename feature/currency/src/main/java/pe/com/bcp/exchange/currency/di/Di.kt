package pe.com.bcp.exchange.currency.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pe.com.bcp.exchange.core.domain.entity.Exchange
import pe.com.bcp.exchange.core.domain.entity.ExchangeRate
import pe.com.bcp.exchange.core.domain.repository.CurrencyRepository
import pe.com.bcp.exchange.core.domain.usecase.GetExchangeRatesUseCase
import pe.com.bcp.exchange.core.domain.usecase.UseCase
import pe.com.bcp.exchange.currency.data.datasource.GetExchangeRatesDataSource
import pe.com.bcp.exchange.currency.data.datasource.local.LocalGetExchangeRatesDataSource
import pe.com.bcp.exchange.currency.data.repository.CurrencyRepositoryImpl
import pe.com.bcp.exchange.currency.ui.converter.CurrencyConverterViewModel
import pe.com.bcp.exchange.currency.ui.selector.CurrencySelectorViewModel

val uiLayer = module {
    viewModel { CurrencyConverterViewModel() }
    viewModel {
        CurrencySelectorViewModel(
            get()
        )
    }
}

val domainLayer = module {
    factory { data -> ExchangeRate(data = data.get()) }
    factory { data -> Exchange(exchangeRate = data.get()) }
    single<UseCase<Any, Map<String, Map<String, Triple<String, Double, String>>>>> {
        GetExchangeRatesUseCase(
            get()
        )
    }
}

val dataLayer = module {
    single<CurrencyRepository> { CurrencyRepositoryImpl(get()) }
    single<GetExchangeRatesDataSource> { LocalGetExchangeRatesDataSource() }
}

val currencyDiModule = listOf(
    uiLayer, domainLayer, dataLayer
)