package pe.com.bcp.exchange.currency.ui.converter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import pe.com.bcp.exchange.core.domain.entity.Currency
import pe.com.bcp.exchange.core.ui.viewmodel.MainViewModel
import pe.com.bcp.exchange.currency.databinding.FragmentCurrencyConverterBinding

class CurrencyConverterFragment : Fragment() {
    private val viewModel: CurrencyConverterViewModel by viewModel()
    private val mainViewModel: MainViewModel by sharedViewModel()
    private lateinit var binding: FragmentCurrencyConverterBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentCurrencyConverterBinding.inflate(inflater, container, false)
        setupUi()
        subscribeUi()
        return binding.root
    }

    private fun subscribeUi() {
        binding.lifecycleOwner = this
        binding.viewModel = this.viewModel
        binding.mainViewModel = this.mainViewModel
        binding.executePendingBindings()
        mainViewModel.rates.observe(viewLifecycleOwner, ::handlerRates)
    }

    private fun handlerCompute(compute: Boolean?) {
        compute?.let { required ->
            if (required) {
                viewModel.calculateCurrentRate()
            }
        }
    }

    private fun handlerSender(sender: Currency?) {
        sender?.let { viewModel.updateSender(it) }
    }

    private fun handlerReceiver(receiver: Currency?) {
        receiver?.let { viewModel.updateReceiver(it) }
    }

    private fun handlerRates(data: Map<String, Map<String, Triple<String, Double, String>>>?) {
        data?.let {
            viewModel.submitRates(it)
            mainViewModel.sender.observe(viewLifecycleOwner, ::handlerSender)
            mainViewModel.receiver.observe(viewLifecycleOwner, ::handlerReceiver)
            mainViewModel.needCompute.observe(viewLifecycleOwner, ::handlerCompute)
            viewModel.computedResult.observe(viewLifecycleOwner, ::updateReceiverData)
            viewModel.calculateCurrentRate()
        }
    }

    private fun setupUi() {
        currencyClickListener()
        setupButtonConverter()
    }

    private fun currencyClickListener() {
        binding.senderCurrencyText.setOnLongClickListener { view ->
            view.tag.toString().let { currency ->
                mainViewModel.isSender(true)
                navigate(currency)
            }
            true
        }

        binding.receiverCurrencyText.setOnLongClickListener { view ->
            view.tag.toString().let { currency ->
                mainViewModel.isSender(false)
                navigate(currency)
            }
            true
        }
    }

    private fun navigate(currency: String) {
        val action = CurrencyConverterFragmentDirections.actionConverterToSelector(currency)
        findNavController().navigate(action)
    }

    private fun updateReceiverData(computedValue: Double?) {
        binding.receiverEdit.setText(computedValue.toString())
    }

    private fun setupButtonConverter() {
        binding.converterButton.setOnClickListener {
            viewModel.compute(
                binding.senderInput.editText?.text.toString().toDouble(),
            )
        }
    }
}