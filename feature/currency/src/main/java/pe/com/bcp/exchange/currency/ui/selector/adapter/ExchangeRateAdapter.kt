package pe.com.bcp.exchange.currency.ui.selector.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import pe.com.bcp.exchange.core.ui.CurrencyRatesItem

class ExchangeRateAdapter(private val block: (currency: CurrencyRatesItem) -> Unit) :
    ListAdapter<CurrencyRatesItem, ExchangeRateViewHolder>(
        ExchangeRateDiffCallback()
    ) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExchangeRateViewHolder {
        return ExchangeRateViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ExchangeRateViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(item) { block.invoke(item) }
    }

}

class ExchangeRateDiffCallback : DiffUtil.ItemCallback<CurrencyRatesItem>() {
    override fun areItemsTheSame(oldItem: CurrencyRatesItem, newItem: CurrencyRatesItem): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(
        oldItem: CurrencyRatesItem,
        newItem: CurrencyRatesItem
    ): Boolean {
        return oldItem == newItem
    }

}