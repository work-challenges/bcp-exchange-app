package pe.com.bcp.exchange.currency.data.datasource

import kotlinx.coroutines.flow.Flow

interface GetExchangeRatesDataSource {
    operator fun invoke(): Flow<Map<String, Map<String, Triple<String, Double, String>>>>
}