package pe.com.bcp.exchange.currency.ui.selector

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.collect
import pe.com.bcp.exchange.core.domain.usecase.UseCase
import pe.com.bcp.exchange.core.ui.CurrencyRatesItem
import pe.com.bcp.exchange.core.ui.viewmodel.BaseViewModel

class CurrencySelectorViewModel(
    private val getExchangeRatesUseCase: UseCase<Any, Map<String, Map<String, Triple<String, Double, String>>>>
) : BaseViewModel() {
    private val _rates = MutableLiveData<List<CurrencyRatesItem>>()
    val rates: LiveData<List<CurrencyRatesItem>> get() = _rates

    fun findCurrencyRates(currency: String) {
        execute {
            getExchangeRatesUseCase.execute(Any()).collect { data ->
                data[currency]?.let { filterData ->
                    val list = mutableListOf<CurrencyRatesItem>()
                    filterData.forEach {
                        list.add(CurrencyRatesItem(currency, it))
                    }
                    _rates.postValue(list)
                }
            }
        }
    }
}