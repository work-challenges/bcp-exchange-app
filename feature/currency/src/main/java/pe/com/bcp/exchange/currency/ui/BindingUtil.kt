package pe.com.bcp.exchange.currency.ui

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import pe.com.bcp.exchange.core.ui.CurrencyRatesItem
import pe.com.bcp.exchange.currency.R

@BindingAdapter("setExchangeRate")
fun TextView.setExchangeRate(rate: Double) {
    text = context.getString(R.string.currency_converter_exchange_rate_value, rate, rate * 1.05)
}

@BindingAdapter("currencyRate")
fun TextView.currencyRate(rateItem: CurrencyRatesItem) {
    val selectedCurrency = rateItem.currency
    val currencyRateValue = rateItem.rate.value.second
    val currencyRateName = rateItem.rate.key
    text = context.getString(
        R.string.item_currency_rate,
        selectedCurrency,
        currencyRateValue,
        currencyRateName
    )
}

@BindingAdapter("loadUrl")
fun loadUrl(image: ImageView, url: String?) {
    url?.let {
        Glide.with(image)
            .load(it)
            .circleCrop()
            .into(image)
    }
}