package pe.com.bcp.exchange.currency.ui.selector.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import pe.com.bcp.exchange.core.ui.CurrencyRatesItem
import pe.com.bcp.exchange.currency.databinding.ItemCurrencyRateBinding

class ExchangeRateViewHolder(
    private val binding: ItemCurrencyRateBinding
) : RecyclerView.ViewHolder(binding.root) {

    companion object {
        fun from(parent: ViewGroup): ExchangeRateViewHolder {
            val binding = ItemCurrencyRateBinding.inflate(LayoutInflater.from(parent.context))
            return ExchangeRateViewHolder(binding)
        }
    }

    fun bind(item: CurrencyRatesItem, block: (view: View) -> Unit) {
        binding.currencyRatesItem = item
        binding.setClickListener { block.invoke(it) }
        binding.executePendingBindings()
    }
}