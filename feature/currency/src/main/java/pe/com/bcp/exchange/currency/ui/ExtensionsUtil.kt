package pe.com.bcp.exchange.currency.ui

import androidx.lifecycle.MutableLiveData
import pe.com.bcp.exchange.core.domain.entity.Currency


fun MutableLiveData<MutableList<Currency>>.toggle(): MutableList<Currency> {
    return value?.reversed()?.toMutableList() ?: mutableListOf()
}