package pe.com.bcp.exchange.currency.data.datasource.local

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import pe.com.bcp.exchange.currency.data.datasource.GetExchangeRatesDataSource

class LocalGetExchangeRatesDataSource : GetExchangeRatesDataSource {
    override fun invoke(): Flow<Map<String, Map<String, Triple<String, Double, String>>>> {
        return flow {
            emit(
                mapOf(
                    "USD" to mapOf(
                        "EUR" to Triple(
                            "European Union",
                            1.44,
                            "https://cdn.countryflags.com/thumbs/europe/flag-800.png"
                        ),
                        "PEN" to Triple(
                            "Peru",
                            3.67,
                            "https://www.countryflags.com/wp-content/uploads/peru-flag-png-large.png"
                        ),
                        "GBR" to Triple(
                            "United Kingdom",
                            3.66,
                            "https://cdn.countryflags.com/thumbs/united-kingdom/flag-800.png"
                        ),
                        "CHF" to Triple(
                            "Switzerland",
                            4.77,
                            "https://www.countryflags.com/wp-content/uploads/switzerland-flag-png-large.png"
                        ),
                        "CAD" to Triple(
                            "Canada",
                            0.88,
                            "https://www.countryflags.com/wp-content/uploads/canada-flag-png-large.png"
                        ),
                        "JPY" to Triple(
                            "Japan",
                            2.22,
                            "https://cdn.countryflags.com/thumbs/japan/flag-800.png"
                        ),
                    ),
                    "PEN" to mapOf(
                        "EUR" to Triple(
                            "European Union",
                            1.44,
                            "https://cdn.countryflags.com/thumbs/europe/flag-800.png"
                        ),
                        "USD" to Triple(
                            "United State",
                            0.55,
                            "https://www.countryflags.com/wp-content/uploads/united-states-of-america-flag-png-large.png"
                        ),
                        "GBR" to Triple(
                            "United Kingdom",
                            0.66,
                            "https://cdn.countryflags.com/thumbs/united-kingdom/flag-800.png"
                        ),
                        "CHF" to Triple(
                            "Switzerland",
                            4.77,
                            "https://www.countryflags.com/wp-content/uploads/switzerland-flag-png-large.png"
                        ),
                        "CAD" to Triple(
                            "Canada",
                            0.88,
                            "https://www.countryflags.com/wp-content/uploads/canada-flag-png-large.png"
                        ),
                        "JPY" to Triple(
                            "Japan",
                            2.22,
                            "https://cdn.countryflags.com/thumbs/japan/flag-800.png"
                        ),
                    ),
                    "EUR" to mapOf(
                        "USD" to Triple(
                            "United State",
                            11.44,
                            "https://www.countryflags.com/wp-content/uploads/united-states-of-america-flag-png-large.png"
                        ),
                        "PEN" to Triple(
                            "Peru",
                            22.55,
                            "https://www.countryflags.com/wp-content/uploads/peru-flag-png-large.png"
                        ),
                        "GBR" to Triple(
                            "United Kingdom",
                            2.66,
                            "https://cdn.countryflags.com/thumbs/united-kingdom/flag-800.png"
                        ),
                        "CHF" to Triple(
                            "Switzerland",
                            44.77,
                            "https://www.countryflags.com/wp-content/uploads/switzerland-flag-png-large.png"
                        ),
                        "CAD" to Triple(
                            "Canada",
                            3.88,
                            "https://www.countryflags.com/wp-content/uploads/canada-flag-png-large.png"
                        ),
                        "JPY" to Triple(
                            "Japan",
                            2.22,
                            "https://cdn.countryflags.com/thumbs/japan/flag-800.png"
                        ),
                    ),

                    "GBR" to mapOf(
                        "EUR" to Triple(
                            "European Union",
                            1.44,
                            "https://cdn.countryflags.com/thumbs/europe/flag-800.png"
                        ),
                        "PEN" to Triple(
                            "Peru",
                            0.55,
                            "https://www.countryflags.com/wp-content/uploads/peru-flag-png-large.png"
                        ),
                        "USD" to Triple(
                            "United State",
                            3.66,
                            "https://www.countryflags.com/wp-content/uploads/united-states-of-america-flag-png-large.png"
                        ),
                        "CHF" to Triple(
                            "Switzerland",
                            47.77,
                            "https://www.countryflags.com/wp-content/uploads/switzerland-flag-png-large.png"
                        ),
                        "CAD" to Triple(
                            "Canada",
                            0.88,
                            "https://www.countryflags.com/wp-content/uploads/canada-flag-png-large.png"
                        ),
                        "JPY" to Triple(
                            "Japan",
                            2.22,
                            "https://cdn.countryflags.com/thumbs/japan/flag-800.png"
                        ),
                    ),
                    "CHF" to mapOf(
                        "EUR" to Triple(
                            "European Union",
                            1.44,
                            "https://cdn.countryflags.com/thumbs/europe/flag-800.png"
                        ),
                        "PEN" to Triple(
                            "Peru",
                            2.55,
                            "https://www.countryflags.com/wp-content/uploads/peru-flag-png-large.png"
                        ),
                        "GBR" to Triple(
                            "United Kingdom",
                            0.66,
                            "https://cdn.countryflags.com/thumbs/united-kingdom/flag-800.png"
                        ),
                        "USD" to Triple(
                            "United State",
                            4.77,
                            "https://www.countryflags.com/wp-content/uploads/united-states-of-america-flag-png-large.png"
                        ),
                        "CAD" to Triple(
                            "Canada",
                            3.88,
                            "https://www.countryflags.com/wp-content/uploads/canada-flag-png-large.png"
                        ),
                        "JPY" to Triple(
                            "Japan",
                            2.22,
                            "https://cdn.countryflags.com/thumbs/japan/flag-800.png"
                        ),
                    ),
                    "CAD" to mapOf(
                        "EUR" to Triple(
                            "European Union",
                            1.44,
                            "https://cdn.countryflags.com/thumbs/europe/flag-800.png"
                        ),
                        "PEN" to Triple(
                            "Peru",
                            23.55,
                            "https://www.countryflags.com/wp-content/uploads/peru-flag-png-large.png"
                        ),
                        "GBR" to Triple(
                            "United Kingdom",
                            30.66,
                            "https://cdn.countryflags.com/thumbs/united-kingdom/flag-800.png"
                        ),
                        "CHF" to Triple(
                            "Switzerland",
                            41.77,
                            "https://www.countryflags.com/wp-content/uploads/switzerland-flag-png-large.png"
                        ),
                        "JPY" to Triple(
                            "Japan",
                            2.22,
                            "https://cdn.countryflags.com/thumbs/japan/flag-800.png"
                        ),
                        "USD" to Triple(
                            "United State",
                            1.88,
                            "https://www.countryflags.com/wp-content/uploads/united-states-of-america-flag-png-large.png"
                        ),
                    ),
                    "JPY" to mapOf(
                        "EUR" to Triple(
                            "European Union",
                            1.44,
                            "https://cdn.countryflags.com/thumbs/europe/flag-800.png"
                        ),
                        "PEN" to Triple(
                            "Peru",
                            23.55,
                            "https://www.countryflags.com/wp-content/uploads/peru-flag-png-large.png"
                        ),
                        "GBR" to Triple(
                            "United Kingdom",
                            30.66,
                            "https://cdn.countryflags.com/thumbs/united-kingdom/flag-800.png"
                        ),
                        "CHF" to Triple(
                            "Switzerland",
                            41.77,
                            "https://www.countryflags.com/wp-content/uploads/switzerland-flag-png-large.png"
                        ),
                        "CAD" to Triple(
                            "Canada",
                            2.12,
                            "https://www.countryflags.com/wp-content/uploads/canada-flag-png-large.png"
                        ),
                        "USD" to Triple(
                            "United State",
                            1.88,
                            "https://www.countryflags.com/wp-content/uploads/united-states-of-america-flag-png-large.png"
                        ),
                    ),
                )
            )
        }
    }
}