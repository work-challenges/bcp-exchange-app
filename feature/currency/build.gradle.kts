plugins {
    id("com.android.library")
    id("kotlin-android")
    id("kotlin-kapt")
    id("androidx.navigation.safeargs")
}

android {
    compileSdk = 31
    defaultConfig {
        minSdk = 24
        targetSdk = 31
        testInstrumentationRunner = AndroidConfig.testRunner
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        getByName("release") {
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    dataBinding {
        isEnabled = true
    }
    viewBinding {
        isEnabled = true
    }
}

dependencies {
    api(project(":common:core"))
    implementation(Dependency.glide)
    implementation(Dependency.navigationFragmentKtx)
    implementation(Dependency.navigationUiKtx)

    kapt(Dependency.compilerGlide)

    testImplementation(Dependency.testJunit)
    testImplementation(Dependency.testTruth)

    androidTestImplementation(Dependency.testAndroidJunit)
}