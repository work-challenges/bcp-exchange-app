object Dependency {
    const val appcompat = "androidx.appcompat:appcompat:${Version.appcompatVersion}"
    const val compilerGlide = "com.github.bumptech.glide:compiler:${Version.glideCompilerVersion}"
    const val compilerRoom = "androidx.room:room-compiler:${Version.roomVersion}"
    const val constraintlayout =
        "androidx.constraintlayout:constraintlayout:${Version.constraintLayoutVersion}"
    const val coreKtx = "androidx.core:core-ktx:${Version.coreKtxVersion}"
    const val navigationFragmentKtx =
        "androidx.navigation:navigation-fragment-ktx:${Version.navigationFragmentVersion}"
    const val firebaseBom = "com.google.firebase:firebase-bom:${Version.firebaseBomVersion}"
    const val firebaseDynamicLinks = "com.google.firebase:firebase-dynamic-links-ktx"
    const val firebaseAnalytics = "com.google.firebase:firebase-analytics-ktx"
    const val glide = "com.github.bumptech.glide:glide:${Version.glideVersion}"
    const val koinAndroid = "io.insert-koin:koin-android:${Version.koinVersion}"
    const val koinCore = "io.insert-koin:koin-core:${Version.koinVersion}"
    const val kotlinxCoroutinesCore =
        "org.jetbrains.kotlinx:kotlinx-coroutines-core:${Version.coroutineCoreVersion}"
    const val legacySupport = "androidx.legacy:legacy-support-v4:${Version.legacySupportVersion}"
    const val material = "com.google.android.material:material:${Version.materialVersion}"
    const val navigationUiKtx =
        "androidx.navigation:navigation-ui-ktx:${Version.navigationUiVersion}"
    const val playServicesAuth =
        "com.google.android.gms:play-services-auth:${Version.googleAuthServicesVersion}"
    const val room = "androidx.room:room-runtime:${Version.roomVersion}"
    const val roomKtx = "androidx.room:room-ktx:${Version.roomVersion}"
    const val squareupGson =
        "com.squareup.retrofit2:converter-gson:${Version.squareupGsonConverterVersion}"
    const val squareupLogging =
        "com.squareup.okhttp3:logging-interceptor:${Version.squareupLoggingVersion}"
    const val squareupRetrofit =
        "com.squareup.retrofit2:retrofit:${Version.squareupRetrofitVersion}"
    const val stetho = "com.facebook.stetho:stetho:${Version.stethoVersion}"
    const val testAndroidJEspressoCore =
        "androidx.test.espresso:espresso-core:${Version.espressoCoreVersion}"
    const val testAndroidJunit = "androidx.test.ext:junit:${Version.androidJunitVersion}"
    const val testAndroidRunner = "androidx.test:runner:${Version.testAndroidRunnerVersion}"
    const val testAndroidRules = "androidx.test:rules:${Version.testAndroidRulesVersion}"
    const val testJunit = "junit:junit:${Version.jUnitVersion}"
    const val testTruth = "com.google.truth:truth:${Version.testTruthVersion}"
    const val viewmodelKtx =
        "androidx.lifecycle:lifecycle-viewmodel-ktx:${Version.viewmodelVersion}"
    const val gson = "com.google.code.gson:gson:${Version.gsonVersion}"
    const val lottie = "com.airbnb.android:lottie:${Version.lottieVersion}"
}