object AndroidConfig {
    const val minSdk = 26
    const val targetSdk = 32
    const val compileSdk = 32
    const val appId = "pe.com.bcp.exchange"
    const val versionCode = 1
    const val versionName = "1.0.0"
    const val testRunner = "androidx.test.runner.AndroidJUnitRunner"
}
