object Plugin {
    const val sageArgsPlugin =
        "androidx.navigation:navigation-safe-args-gradle-plugin:${Version.safeArgsVersion}"
    const val buildToolsPlugin = "com.android.tools.build:gradle:${Version.buildToolsVersion}"
    const val kotlinGradlePlugin =
        "org.jetbrains.kotlin:kotlin-gradle-plugin:${Version.kotlinVersion}"
    const val googleServices = "com.google.gms:google-services:${Version.gsmGoogleServicesVersion}"
}