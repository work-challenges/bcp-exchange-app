package pe.com.bcp.exchange

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import pe.com.bcp.exchange.core.di.coreDiModule
import pe.com.bcp.exchange.currency.di.currencyDiModule

class ExchangeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(coreDiModule)
            modules(currencyDiModule)
            androidLogger()
            androidContext(this@ExchangeApplication)
        }
    }
}