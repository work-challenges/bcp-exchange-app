package pe.com.bcp.exchange.ui.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import pe.com.bcp.exchange.databinding.FragmentSplashBinding

private const val DURATION = 4000L

class SplashFragment : Fragment() {
    lateinit var binding: FragmentSplashBinding

    override fun onCreateView(
        inflater: LayoutInflater, parent: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSplashBinding.inflate(inflater, parent, false)
        return binding.root
    }

    override fun onResume() {
        super.onResume()
        Handler(Looper.getMainLooper()).postDelayed(::navigate, DURATION)
    }

    private fun navigate() {
        val action = SplashFragmentDirections.actionSplashFragmentToCurrencyNav()
        findNavController().navigate(action)
    }
}