plugins {
    id("com.android.library")
    id("org.jetbrains.kotlin.android")
}

android {
    compileSdk = 32

    defaultConfig {
        minSdk = 26
        targetSdk = 32

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        consumerProguardFiles("consumer-rules.pro")
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {
    api(Dependency.appcompat)
    api(Dependency.material)
    api(Dependency.coreKtx)
    api(Dependency.constraintlayout)
    api(Dependency.gson)
    api(Dependency.kotlinxCoroutinesCore)
    api(Dependency.navigationFragmentKtx)
    api(Dependency.navigationUiKtx)
    api(Dependency.koinCore)
    api(Dependency.viewmodelKtx)
    api(Dependency.koinAndroid)
    api(Dependency.squareupLogging)
    api(Dependency.squareupRetrofit)
    api(Dependency.squareupGson)

    testImplementation(Dependency.testJunit)
    testImplementation(Dependency.testTruth)
    androidTestImplementation(Dependency.testAndroidJunit)
}