package pe.com.bcp.exchange.core.domain.entity

class ExchangeRate(private val data: Map<String, Map<String, Triple<String, Double, String>>>) {

    fun getExchangeRate(senderName: String, receiverName: String): Double {
        return data[senderName]?.get(receiverName)?.second ?: 0.0
    }
}