package pe.com.bcp.exchange.core.domain.usecase

import kotlinx.coroutines.flow.Flow

interface UseCase<in I : Any, out O : Any> {
    suspend fun execute(input: I): Flow<O>
}