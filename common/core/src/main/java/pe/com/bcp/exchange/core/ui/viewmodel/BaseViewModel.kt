package pe.com.bcp.exchange.core.ui.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

abstract class BaseViewModel : ViewModel() {
    private val _failure: MutableLiveData<Exception> = MutableLiveData()

    /**
     * Subscribe to [failure] to be notified with error message
     */
    val failure: LiveData<Exception> get() = _failure

    /**
     * This method help you execute coroutine.
     * It also handle errors.
     */
    protected fun execute(block: suspend () -> Unit): Job = viewModelScope.launch {
        try {
            block.invoke()
            Log.d(BaseViewModel::class.java.simpleName, "executing use case...")
        } catch (e: Exception) {
            Log.d(BaseViewModel::class.java.simpleName, "failure use case cause...${e.message}")
            _failure.postValue(e)
        }
    }
}