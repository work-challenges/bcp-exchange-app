package pe.com.bcp.exchange.core.domain.entity

import pe.com.bcp.exchange.core.domain.entity.Country.*

object CurrencyFactory {
    fun create(country: Country): Currency = when (country) {
        PERU -> Currency(
            1,
            "PEN",
            "Sol",
            "Peru",
            "https://www.countryflags.com/wp-content/uploads/peru-flag-png-large.png"
        )
        UNITED_STATE -> Currency(
            2,
            "USD",
            "Dollar",
            "United State",
            "https://www.countryflags.com/wp-content/uploads/united-states-of-america-flag-png-large.png"
        )
        JAPAN -> Currency(
            3,
            "JPY",
            "Yen",
            "Japan",
            "https://cdn.countryflags.com/thumbs/japan/flag-800.png"
        )
        UNITED_KINGDOM -> Currency(
            4,
            "GBR",
            "Pound sterling",
            "United Kingdom",
            "https://cdn.countryflags.com/thumbs/united-kingdom/flag-800.png"
        )
        SWITZERLAND -> Currency(
            5,
            "CHF",
            "Franco suizo",
            "Switzerland",
            "https://www.countryflags.com/wp-content/uploads/switzerland-flag-png-large.png"
        )
        CANADA -> Currency(
            6,
            "CAD",
            "Canadian",
            "Canada",
            "https://www.countryflags.com/wp-content/uploads/canada-flag-png-large.png"
        )
        EUROPEAN_UNION -> Currency(
            7,
            "EUR",
            "Euro",
            "European union",
            "https://cdn.countryflags.com/thumbs/europe/flag-800.png"
        )
    }
}