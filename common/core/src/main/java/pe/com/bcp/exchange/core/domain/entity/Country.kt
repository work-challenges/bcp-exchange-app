package pe.com.bcp.exchange.core.domain.entity

enum class Country(val currency: String) {
    PERU("PEN"),
    UNITED_STATE("USD"),
    JAPAN("JPY"),
    UNITED_KINGDOM("GBP"),
    SWITZERLAND("CHF"),
    CANADA("CAD"),
    EUROPEAN_UNION("EUR");

    companion object {
        fun findByCurrency(currency: String): Country {
            return values().find { it.currency == currency } ?: EUROPEAN_UNION
        }
    }
}