package pe.com.bcp.exchange.core.domain.repository

import kotlinx.coroutines.flow.Flow

interface CurrencyRepository {
    fun getExchangeRates(): Flow<Map<String, Map<String, Triple<String, Double, String>>>>
}