package pe.com.bcp.exchange.core.ui.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.flow.collect
import org.koin.core.component.KoinComponent
import pe.com.bcp.exchange.core.domain.entity.Country
import pe.com.bcp.exchange.core.domain.entity.Currency
import pe.com.bcp.exchange.core.domain.entity.CurrencyFactory
import pe.com.bcp.exchange.core.domain.usecase.UseCase
import pe.com.bcp.exchange.core.ui.CurrencyRatesItem

class MainViewModel(
    private val getExchangeRatesUseCase: UseCase<Any, Map<String, Map<String, Triple<String, Double, String>>>>
) : BaseViewModel(), KoinComponent {
    private val _rates = MutableLiveData<Map<String, Map<String, Triple<String, Double, String>>>>()
    private val _senderCurrency = MutableLiveData(CurrencyFactory.create(Country.UNITED_STATE))
    private val _receiverCurrency = MutableLiveData(CurrencyFactory.create(Country.PERU))
    private val _needCompute = MutableLiveData(false)

    val sender: LiveData<Currency> get() = _senderCurrency
    val receiver: LiveData<Currency> get() = _receiverCurrency
    val rates: LiveData<Map<String, Map<String, Triple<String, Double, String>>>> get() = _rates
    val needCompute: LiveData<Boolean> get() = _needCompute

    private var isSender: Boolean = true

    init {
        fetchExchangeRates()
    }

    private fun fetchExchangeRates() {
        execute {
            getExchangeRatesUseCase.execute(Any()).collect { data ->
                this._rates.postValue(data)
            }
        }
    }

    fun updateSelectedCurrency(selected: CurrencyRatesItem) {
        if (isSender) {
            saveSender(
                CurrencyFactory.create(
                    Country.findByCurrency(
                        selected.rate.key
                    )
                )
            )
        } else {
            saveReceiver(
                CurrencyFactory.create(
                    Country.findByCurrency(
                        selected.rate.key
                    )
                )
            )
        }
    }

    fun isSender(isSender: Boolean) {
        this.isSender = isSender
    }

    fun toggle() {
        mutableListOf(
            sender.value!!,
            receiver.value!!
        ).reversed().apply {
            saveSender(first())
            saveReceiver(last())
            _needCompute.postValue(true)
        }
    }

    private fun saveReceiver(receiver: Currency) {
        isSender(false)
        _receiverCurrency.postValue(receiver)
    }

    private fun saveSender(sender: Currency) {
        isSender(true)
        _senderCurrency.postValue(sender)
    }

}