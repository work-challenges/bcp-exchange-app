package pe.com.bcp.exchange.core.domain.entity

data class Currency(
    val id: Long,
    val name: String,
    val description: String,
    val countryName: String,
    val countryFlag: String
)