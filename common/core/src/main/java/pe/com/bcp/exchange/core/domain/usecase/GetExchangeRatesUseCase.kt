package pe.com.bcp.exchange.core.domain.usecase

import kotlinx.coroutines.flow.Flow
import pe.com.bcp.exchange.core.domain.repository.CurrencyRepository

class GetExchangeRatesUseCase(
    private val currencyRepository: CurrencyRepository
) : UseCase<Any, Map<String, Map<String, Triple<String, Double, String>>>> {

    override suspend fun execute(input: Any): Flow<Map<String, Map<String, Triple<String, Double, String>>>> {
        return currencyRepository.getExchangeRates()
    }
}