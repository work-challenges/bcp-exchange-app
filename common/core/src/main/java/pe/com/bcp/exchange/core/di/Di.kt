package pe.com.bcp.exchange.core.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import pe.com.bcp.exchange.core.ui.viewmodel.MainViewModel

val uiLayer = module {
    viewModel {
        MainViewModel(get())
    }
}

val coreDiModule = listOf(
    uiLayer
)
