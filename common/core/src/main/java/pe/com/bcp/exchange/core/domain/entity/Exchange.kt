package pe.com.bcp.exchange.core.domain.entity

class Exchange(private val exchangeRate: ExchangeRate) {
    fun compute(senderAmount: Double, senderName: String, receiverName: String): Double {
        val rate = exchangeRate.getExchangeRate(senderName, receiverName)
        return senderAmount * rate
    }
}