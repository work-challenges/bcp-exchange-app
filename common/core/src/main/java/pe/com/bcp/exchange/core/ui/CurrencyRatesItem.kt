package pe.com.bcp.exchange.core.ui

import java.io.Serializable

data class CurrencyRatesItem(
    val currency: String,
    val rate: Map.Entry<String, Triple<String, Double, String>>
) : Serializable